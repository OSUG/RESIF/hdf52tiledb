#!/usr/bin/env python3

import tiledb
import h5py
import time
import logging
import click
import numpy as np

logging.basicConfig()
logger = logging.getLogger(__name__)


def convert_ph5():
   for idf, f in enumerate(flist[:10]):
       zname = f.split("_")[-1][:-3]
       print(f"{idf+1}: working on {zname}")
       f = h5py.File(f,'r')

       with tiledb.open(f"{bucket}/RawData", 'w', ctx = ctx) as A:
           A[:, idf * 12000 : (idf+1) * 12000] = f['/Acquisition/Raw[0]/RawData'][:, :]
       f.close()



def tiledb_make_context():
    config = tiledb.Config()
    # Set configuration parameters
    config["vfs.s3.scheme"] = "http"
    config["vfs.s3.region"] = "Europe/France"
    config["vfs.s3.endpoint_override"] = "resif-minio.u-ga.fr:9000"
    config["vfs.s3.use_virtual_addressing"] = "false"
    config["vfs.s3.aws_access_key_id"] = "this-is-key"
    config["vfs.s3.aws_secret_access_key"] = "this-is-secret"
    config["sm.consolidation.mode"] = "fragment_meta"
    config["sm.vacuum.mode"] = "fragment_meta"

    # Create contex
    ctx = tiledb.Ctx(config)

    bucket = f"s3://TileDB-OOI-DAS-{tile_size}/"

    dim1 = tiledb.Dim(name="time", domain=(0, 200*60*60*24*7), tile=12000, dtype=np.uint32,
                    filters = tiledb.FilterList([tiledb.DoubleDeltaFilter(),tiledb.GzipFilter(level=-1)]))
    dim2 = tiledb.Dim(name="channel", domain=(0, 47500-1), tile=tile_size, dtype=np.uint32,
                    filters = tiledb.FilterList([tiledb.DoubleDeltaFilter(),tiledb.GzipFilter(level=-1)]))

    dom = tiledb.Domain(dim2, dim1)
    rawdata = tiledb.Attr(name="RawData", dtype=np.int32,
                        filters=tiledb.FilterList([tiledb.ByteShuffleFilter(), tiledb.LZ4Filter(level = 5)]))
    schema = tiledb.ArraySchema(domain=dom, sparse=False, attrs=[rawdata])
    tiledb.Array.create(f"{bucket}/RawData/", schema = schema, ctx=ctx)



@click.command()
@click.option('--verbose', '-v', is_flag=True, default=False, help='Number of greetings.')
@click.option('--ph5dir', type=click.Path(dir_okay=True, readable=True), help="Path to PH5 directory",)
def main(ph5dir, verbose):
    if verbose:
        logger.setLevel(logging.debug)
    logger.debug("Starting ph52tiledb")



if __name__ == '__main__':
    main()
